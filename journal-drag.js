
JournalDrag = function () {

    localImportFromCollection = async function(collection, entryId, updateData={}, options = {}) {
        const entName = this.object.entity;
        const pack = game.packs.find(p => p.collection === collection);
        if ( pack.metadata.entity !== entName ) return;

        // Get the entry from the pack
        const entity = await pack.getEntity(entryId);
        const name = entity.name;

        // Does the name already exist
        existing = this.entities.find((ent) => name === ent.data.name);
        
        if(!existing) {
            const createData = mergeObject(entity.data, updateData);
            delete createData._id;

            // Create and return a Promise
            console.log(`${vtt} | Importing ${entName} ${entity.name} from ${collection}`);
            entity.constructor.create(createData, options);
        }
        return name;
    }

    setDraggable = (html) => {
        html.find(".entity-link").each((i, elt) => {
            elt.setAttribute("draggable", true);
            elt.addEventListener('dragstart', _onDragStart, false);
        });
    }

    sheetHook = (app, html, data)  => {
        if (!game.settings.get('journal-drag', 'enableCharacterSheet')) return;
        setDraggable(html);
        let editor = html.find(".editor-edit");
        if (editor.length > 0) {
            editor[0].addEventListener("click", (event) =>
            app.editors["data.details.biography.value"].mce.on("drop", doDrop.bind(null, app.editors["data.details.biography.value"].mce)));
        }
    }

    journalHook = (app, html, data) => {
        if (!game.settings.get('journal-drag', 'enableJournalSheet')) return;
        setDraggable(html);
        let editor = html.find(".editor-edit");
        if (editor.length > 0) {
            editor[0].addEventListener("click", (event) => app.editors.content.mce.on("drop", doDrop.bind(null, app.editors.content.mce)));
        }
    }

    const collections = {"Actor": Actor, "Item": Item, JournalEntry: JournalEntry, "Scene": Scene, "RollTable": RollTable};
    doDrop = async(editor, event)=> {
        try {
            data = JSON.parse(event.dataTransfer.getData("text"));
            console.log(data);
            if (collections[data.type]) {
                event.preventDefault();
                let name = data.pack ?
                    await localImportFromCollection.bind(collections[data.type].collection)(data.pack, data.id, {}, {displaySheet: false})
                    : collections[data.type].collection.get(data.id).data.name;
                newText = `@${data.type}[${name}]`;
                editor.insertContent(newText);
            }
        } catch(err) {
            console.log(event.dataTransfer.getData("text"));
            console.warn(err);
        }
    }

    _onDragStart = (ev) => {
        const dragData = {type: ev.path[0].attributes[1].nodeValue, id: ev.path[0].attributes[2].nodeValue};
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }
    
    return {journalHook: journalHook, sheetHook: sheetHook}
}()

Hooks.once("ready", () => {
    for (let option of [
        {
            settingName: "enableJournalSheet",
            name: game.i18n.localize("journal-drag.enableJournalSheet.Name"),
            hint: game.i18n.localize("journal-drag.enableJournalSheet.Hint"),
            scope: "client",
            config: true,
            default: true,
            type: Boolean, 
        },
        {
            settingName: "enableCharacterSheet",
            name: game.i18n.localize("journal-drag.enableCharacterSheet.Name"),
            hint: game.i18n.localize("journal-drag.enableCharacterSheet.Hint"),
            scope: "client",
            config: true,
            default: true,
            type: Boolean
        }
    ]) {
        game.settings.register("journal-drag", option.settingName, option);
    };
    Hooks.on("renderJournalSheet", JournalDrag.journalHook);
    Hooks.on("renderActorSheet5eCharacter", JournalDrag.sheetHook);
    Hooks.on("renderActorSheet5eNPC", JournalDrag.sheetHook);
});