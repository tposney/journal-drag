# Journal Drag

This little module allows you to do two things, 1) drag links from the journal to other places; actors to the map, items to characters and of course actors, items, journal entries, rollable tables amd scemes to other jounral entries and 2) drag items/actors/journal entries/tables/scenes into a journal entry - so you don't need to type the exact name into the link entry in the journal.

### Installation Instructions

To install the module, follow these instructions:
1. From foundry select install module from the Add-on modules tab of the setup screen.
1. Paste the url for the module.json https://gitlab.com/tposney/journal-drag/raw/master/module.json into the manifest UIRL field and press install.

OR do a direct download of the module:

1. [Download the zip]https://gitlab.com/tposney/journal-drag/raw/master/journal-drag.zip file included in the module directory.
2. Extract the included folder to `public/modules` in your Foundry Virtual Tabletop installation folder.
3. Restart Foundry Virtual Tabletop.  
4. Since this module includes a compendium of items the first time you run with it installed, foundry will complain and not let you open the compendium. Simply return to setup and enter the world a second time and all should be good.

### Notes
* Dragging from the journal works when the journal text entry IS NOT being editied.
* Dragging to the journal works when the journal text entry IS being edited. The MCE editor is a little bit fussy, so position the cursor where you want to drop the linked item before doing the drop.
* When dragging from the journal press and hold the link you want to drag for a short while (so that the automatic open of the link does not fire) and then drag it to where you want to place it.
* v 0.3 adds dropping from compendia. Dropped entities are imported if no entity of the matching type with the same name exists.